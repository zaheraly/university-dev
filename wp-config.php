<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Add any custom values between this line and the "stop editing" line. */



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}


define('AUTH_KEY',         'T+jsdGkMpylLIFqdHQYnDlu/38xjFJOadfoDYyO0wny8vXz63aGuM+NN+FNTAlBuzLASPTL5fKOq4V2Q2TjdOg==');
define('SECURE_AUTH_KEY',  'Xmngmtgg7EhtKh0JHjKm9GB8sEiSTtFtwEh/kt58bsoaqU085Db3tqa96tJS+K7BsuPLpGESeBF7I0dY/bhYag==');
define('LOGGED_IN_KEY',    'yJ2AT/0FfQ5Q4GknsjivJn5WGFQkeEIPUl1zay0oyBINkw8nd8u25DgqYluccu86qRZHV9BIb2VDAQ8oEA3P8Q==');
define('NONCE_KEY',        'jZhrIydvW1udWscuUDdsx7Y84oKzwD5ijgcY1ahYke+Hmbr/3ezrdnSStl8FHuxqnqgpxxfb+P/sZfoGe+76iQ==');
define('AUTH_SALT',        's1S4Oz2p81mUVlga3sPwVOmuPwHJMCg5VWCl+3wBoe5zIlq5ITRDCItJJqmgzTy/57Y7aswxA/RMsUuW5yZvtA==');
define('SECURE_AUTH_SALT', 'CYzTrrVeGVpWgrcHd+iKA2sSLikCzwGF+/4L0jxPbejxy/P3wU6ORFvhEARTSd8NHiVuj8ntZNRBzcXSVSFKuQ==');
define('LOGGED_IN_SALT',   'Pzrq45SW5lbGTrX4ktB2Kg+xPjByQlVeHWNub7sI26AEuRyYiwh2yoz2jlIVcY26ISX4a0Oc996Mj2075MB2nw==');
define('NONCE_SALT',       'djDQ0orASAfDRlhp053TxMdoTKKKKVs3xxPGbKzhQ6ntrStl+fqFX4O3tyLZw6H4Rd+DUMehgeA8DhmFSjN1ew==');
define( 'WP_ENVIRONMENT_TYPE', 'local' );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
